package gettitle

import (
	"encoding/json"
	"io/ioutil"
	"net/http"
	"time"

	"github.com/sirupsen/logrus"
)

var logger = logrus.WithField("logger", "GetTitle")

const timeoutDuration = 10

type icecast struct {
	Artist string
	Title  string
}

//GetCurentTitle get the current title from Icecast server
func GetCurentTitle() (string, string) {
	//get the json
	netClient := &http.Client{
		Timeout: time.Second * timeoutDuration,
	}
	resp, err := netClient.Get("https://my.super/api/endpoint/curenttitle")
	if err != nil {
		logger.Error(err)
	}

	defer func() {
		if err := resp.Body.Close(); err != nil {
			logger.Error("Error closing Icecast connection")
		}
	}()

	data, err := ioutil.ReadAll(resp.Body)

	icecastData := &icecast{}
	err = json.Unmarshal(data, icecastData)

	return icecastData.Artist, icecastData.Title
}
