package main

import (
	"os"
	"os/signal"
	"syscall"

	"../milleniumtwitchbot/gettitle"

	"github.com/gempir/go-twitch-irc"
	"github.com/sirupsen/logrus"
)

var logger = logrus.WithField("logger", "main")

func main() {

	client := twitch.NewClient("tomaveclevdsl", "oauth:MyCoolOauthKey")

	client.OnPrivateMessage(func(message twitch.PrivateMessage) {
		logger.Info(message.User.DisplayName + " : " + message.Message)
		if message.Message == "?musique" {
			artist, title := gettitle.GetCurentTitle()
			client.Say("stationmillenium", "Vous ecoutez : "+title+" - "+artist)
		}
	})

	client.Join("stationmillenium")

	err := client.Connect()
	if err != nil {
		panic(err)
	}

	c := make(chan os.Signal)
	signal.Notify(c, os.Interrupt, syscall.SIGTERM)
	go func() {
		<-c
		client.Say("stationmillenium", "chatbot Developpé par tomaveclevdsl ")
		os.Exit(1)
	}()

}
